package com.funsoft.InformativoOnline.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="contratos")
public class Contrato extends DefaultModel {

    private int cota;

    private Date dataInicio;

    private Date dataFim;

    @JoinColumn(name="clienteId")
    @JsonBackReference
    private Cliente cliente;

    @JoinColumn(name="planoId")
    @JsonBackReference
    private Plano plano;

    private boolean status;

}
