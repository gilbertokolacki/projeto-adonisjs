package com.funsoft.InformativoOnline.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RetornoResource implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer status;
    private String menssagem;

    public RetornoResource getStatusOk() {
        RetornoResource retorno = new RetornoResource(200, "Processo concluido com Sucesso!");
        return retorno;
    }


    public RetornoResource getStatusError(String menssagem) {
        RetornoResource retorno = new RetornoResource(400, menssagem);
        return retorno;
    }
}
