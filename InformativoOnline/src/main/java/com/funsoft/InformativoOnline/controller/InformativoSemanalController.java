package com.funsoft.InformativoOnline.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//@CrossOrigin(origins = "http://localhost:3031")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/informativoPlugin/api/")
public class InformativoSemanalController {

    @PostMapping("/envioEmailSemanal")
    public RetornoResource insereClientes(@RequestBody @Valid String body) {
        RetornoResource retorno = new RetornoResource();
        try {
            JsonObject request = new JsonParser().parse(body).getAsJsonObject();
            Long idInformativo = request.get("id").getAsLong();
            return retorno.getStatusOk();
        } catch (Exception e) {
            return retorno.getStatusError(e.getMessage());
        }
    }

}
