package com.funsoft.InformativoOnline.repository;

import com.funsoft.InformativoOnline.models.Contrato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratoRepository extends JpaRepository<Contrato, Long> {

    Contrato findById(long id);

}
