package com.funsoft.InformativoOnline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformativoOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(InformativoOnlineApplication.class, args);
	}

}
